Es gibt zwei Scores: 
  * Builder Score (1. Phase + 3. Phase)
  * Breaker Score (2. Phase)

Scoring generell nach TF-IDF-Prinzip, d.h.:
  * Scoring 1. Phase: IDF
  * Scoring 2. Phase: TF-IDF
  * Scoring 3. Phase: IDF

Builder Score: *s* für ein Team *t* über alle Testfälle *c* einer Phase *y*:
  * :math:`s_{build}(t) = s_{1}(t) + s_{3}(t)`

  * :math:`s_{y}(t) = \sum_{\forall c_{y,i}} x(c_{y,i}, t)`
  
  * :math:`x(c, t) = p(c, t) * \frac{\#\ Teams}{\sum_{\forall t_{i}} p(c, t_{i})}`
  
  * :math:`p(c, t) = \begin{cases}1\ \ \ Team\ t\ hat\ Testfall\ c\ bestanden\\0\ \ \ sonst\end{cases}`
  
Breaker Score: *s* für ein Team *t* über alle eingereichten Testfälle *c* des Teams:
  * :math:`s_{break}(t) = \sum_{\forall c_{t}} x(c_{t})`
  
  * :math:`x(c) =  (1 - \frac{\sum_{\forall t_{i}}p(c, t_{i})}{\#\ Teams})*e(c)`
  
  * :math:`p(c, t) = \begin{cases}1\ \ \ Team\ t\ hat\ Testfall\ c\ bestanden\\0\ \ \ sonst\end{cases}`

  * :math:`e(c) = \frac{1}{\#\ Teams \ mit\ Test\ in\ der\ Aequivalenzklasse\ von\ c * \#\ der\ eigenen\ Tests\ in\ der\ Aequivalenzklasse\ von\ c}`