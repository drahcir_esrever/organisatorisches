# Warm Up ~45min (16:00 - 17:15)
* Kurzer Vortrag zur Einführung
    * Was ist der Verein
    * Disclaimer: FAIR PLAY, Wir sind hier um Spaß zu haben! 
    * Einladen aller TN in als Mitglied in Team MatheDual Wettbewerb 2016  
    * Fragen werden auf der Webseite veröffentlicht
    * Erklärung Ablauf Heute
    * Vorstellen des Bewertungsschemas
* Sortierung der Teams im Raum, Aufbau der Laptops
* Einchecken einer Datei in das Repository: 4711.txt
* Einladen aller Teilnehmer in [Bitbucket Team](https://bitbucket.org/mathedual2016/)

# Basisaufgabe ~90min (17:15 - 18:45)
* Veröffentlichung der Basisaufgabe
* ... CODING ...
* Änderungen einchecken
* STOPP!
* Wir laden alle Codes herunter und versuchen während der nächsten Phase zu Kompilieren

# Pause/Puffer ~10min (18:45 - 18:55)

# Erweiterung 1 ~25min (18:55 - 19:20)
* Veröffentlichung der ersten Erweiterung
* ... CODING ...
* STOPP!
* Wir laden alle Codes herunter und versuchen zu Kompilieren

# Pizza ~45min (19:20 - 20:05)
* 50 TN
* Wo hat Denise Pizza bestellt?: https://www.telepizza.de/aachen_burtscheid/party-pizza.html
    * 5 Salami (14,15 EUR)
    * 5 Hawaii (16,35 EUR)
    * 5 Schinken (14,15 EUR)
    * 3 Schinken, Pilze  (17,15 EUR)  
    * 3 Thunfisch Zwiebeln (17,15 EUR)
    * 2 Spinat, Zwiebeln, Paprika (18,55 EUR)
    * 2 Margharita (11,95 EUR)
* 3-4 Kästen Cola, 2-3 Kästen Wasser, 2 Kästen Bier
* Plastikbecher
* Pappteller
* Servietten

# Erweiterung 2 ~25min (20:05 - 20:30)
* Veröffentlichung der zweiten Erweiterung
* ... CODING ...
* STOPP!
* Wir laden alle Codes herunter und versuchen zu Kompilieren

# Pause/Puffer ~10min (20:30 - 20:40)


# Erweiterung 3 ~40min (20:40 - 21:20)
* Veröffentlichung der dritten Erweiterung
* ... CODING ...
* STOPP!
* Wir laden alle Codes herunter und versuchen zu Kompilieren

# Abschluss ~30min (21:20 - 21:50)
* Wo nötig kleinere fixes um alle Codes auszuführen.
* Kurze Besprechung weiteres Vorgehen
    * Veröffentlichung der Codes der anderen Teams als Repository bis Montag(?) 
    * Scoring anhand von unseren Testfällen
        * tc\#\#\#\#.containers.txt, tc\#\#\#\#.ships.txt, tc\#\#\#\#.expected.txt, tc\#\#\#\#.readme.txt
	    * readme enthält Hinweis auf Programme, die mit dem TC nicht korrekt funktionieren.
