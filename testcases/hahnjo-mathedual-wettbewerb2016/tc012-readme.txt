Die Höhe eines Containers ist negativ und sollte deshalb als Fehler erkannt werden. (äquivalent für Breite und Länge)

Unter anderem break_this, mathedual-wettbewerb-2016, mpw0xf und programming-contest schreiben keine Fehlermeldung in die Ausgabedatei.
